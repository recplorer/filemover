package cz.csas.sas_espis.filemover;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sol60831 on 16.5.2016.
 */
public class DB {

    private static final Logger logger = LogManager.getLogger(DB.class);
    private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";


    //private static int int_record_import_count = 0;
    public static int selectRecordsImportCountFake() {
        return 8;
    }


    public static int selectRecordsImportCount() throws SQLException {

        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        int int_record_import_count = 0;
        //jak zajistit kdy se select nepovede aby se nic nestalo?

        String selectSQL = "select count(*) as record_import_count from espis.cs_record_import_run_table where status = 0";

        try {
            dbConnection = getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);

            // execute select SQL statement
            ResultSet rs = preparedStatement.executeQuery();
            //System.out.println(rs.toString());
            while (rs.next()) {
                String record_import_count = rs.getString("record_import_count");
                //String username = rs.getString("USERNAME");
                int_record_import_count = Integer.parseInt(record_import_count);
                logger.info("Running threads count for RecordsImport is: {}", int_record_import_count);

                System.out.println("Count is : " + int_record_import_count);
                //System.out.println("username : " + username);
            }
        } catch (SQLException e) {
            logger.error("cz.csas.sas_espis.filemover.DB ERROR: ", e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return int_record_import_count;
    }

        public static Connection getDBConnection() {
            Connection dbConnection = null;
            try {
                Class.forName(DB_DRIVER);
            } catch (ClassNotFoundException e) {
                logger.error("OJDBC driver not found: ", e.getMessage());
            }
            try {
                dbConnection = DriverManager.getConnection(
                        Properties.DB_CONNECTION, Properties.DB_USER, Properties.DB_PASSWORD);
                return dbConnection;
            } catch (SQLException e) {
                logger.error("Getting cz.csas.sas_espis.filemover.DB connection ERROR: ", e.getMessage());
            }
            return dbConnection;
        }
}
