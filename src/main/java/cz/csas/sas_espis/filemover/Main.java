package cz.csas.sas_espis.filemover;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.nio.file.*;
import java.time.LocalTime;
import java.util.List;
/**
 * Created by srame on 11.05.2016.
 * Spusteni programu
 */

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);
    private static String source_folder = Properties.readProperty("source.folder");
    private static String import_folder = Properties.readProperty("import.folder");
    private static int refresh_time_seconds = Integer.parseInt(Properties.readProperty("refresh.time.seconds")) * 1000;
    private static Boolean db_fake = Boolean.valueOf(Properties.readProperty("db.fake"));
    private static String stop_file = Properties.readProperty("stop.file");

    public static void main(String[] args) throws InterruptedException {

        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                logger.info("Application has been shutdown manually");
            }
        });

        logger.info("Application started.");

        Main.run();

        logger.info("Application stopped.");
    }

    public static void run() {
        while (true) {
            try {

                //kontrola zda existuje stop file. Pokud ano, aplikace se vypne.
                Main.checkStopFile();

                List listOfFiles = MoveFile.listFilesSorted(source_folder);

                int number_of_copied_files = MoveFile.getNumberOfFilesToMove(listOfFiles);
                logger.info("Number of files to copy: {}", number_of_copied_files);

                System.out.println("List of sorted files to copy " + listOfFiles);

                MoveFile.moveFiles(listOfFiles, import_folder, number_of_copied_files);

                try {
                    Thread.sleep(refresh_time_seconds);
                } catch (InterruptedException e) {
                    logger.error("Process interrupted", e);
                }
            } catch (Exception e) {
                logger.error("Problem when running program", e);
            }
        }
    }

    public static Boolean getDBFakeProperty() {
        return db_fake;
    }

    public static void checkStopFile() {
        logger.trace("Checking if stop file '{}' exists.", stop_file);
        Path path = Paths.get(stop_file);

        boolean pathExists = Files.exists(path);

        if (pathExists) {
            logger.info("Stop file '{}' found. Application stopped.", stop_file);
            System.exit(0);
        }
    }

    /**
     * rozhodne zda aktualni cas (v hodinach) je v rozsahu dne nebo noci
     * @return boolean
     */
    public static Boolean isDayHour() {
        int hour;
        Boolean isDayHour = true; //proc to tu musim inicialzovat?
        try {
            LocalTime now = LocalTime.now();
            hour = now.getHour();

            if (hour >= 6 && hour < 22 ) {
                isDayHour = true;
            } else {
                isDayHour = false;
            }

            logger.trace("Current hour value is: {}", hour);
        } catch (Exception e) {
            logger.error("Unable to tell if its day or night hour: ", e);
        } finally {
            if (isDayHour == null) { //kdyz promennou neinicializuju, tak tady je problem
                isDayHour = true;
            }
        }
        return isDayHour;
    }
}
