package cz.csas.sas_espis.filemover;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Paths;
import java.nio.file.*;
import java.util.*;
import java.io.IOException;
import static java.nio.file.StandardCopyOption.*;

/**
 * Created by srame on 11.05.2016.
 * Vylistovani a presunuti souboru
 */
public class MoveFile {

    private static final Logger logger = LogManager.getLogger(MoveFile.class);


    /**
     * Presunuti souboru z listu fileNames do umisteni destination
     * Presunut bude jen pocet souboru filesToCopy
     * @param fileNames
     * @param destination
     * @param filesToCopy
     */
    public static void moveFiles(List<Path> fileNames, String destination, int filesToCopy) {
        Path import_folder = Paths.get(destination);

        try {
            for (int i = 0; i < filesToCopy; i++) {
                Path path = fileNames.get(i);
                    if (!Files.isDirectory(path)) {

                        System.out.println("Moving: " + path.toString()
                                + " to " + import_folder.toString());
                        logger.debug("Moving: " + path.toString()
                                + " to " + import_folder.toString());

                        Path dest_path = Paths.get(import_folder.toString(), path.getFileName().toString());
                        //System.out.println(dest_path);
                        Files.move(path, dest_path, ATOMIC_MOVE);

                        logger.debug("...moving successful.");
                    } else {
                        logger.error("Passed file {} is a directory", path);
                    }
            }
        } catch (AtomicMoveNotSupportedException ex) {
            //System.out.println("Filesystem neodporuje atomic move");
            logger.error("Filesystem doesn't support atomic move ", ex);
        } catch (FileAlreadyExistsException ex) {
            logger.error("File already exists in target folder {}", destination, ex);
        } catch (NoSuchFileException ex) {
            logger.error("Unable to move files, directory {} doesn't exist ", import_folder, ex);
        } catch (Exception ex) {
            logger.error("Unable to move files ", ex);
        } finally {
            logger.info("Moving files finished");
        }
    }

    /**
     * Vylistovani adresare v parametru directory dle mtime
     * @param directory
     * @return
     */
    public static List<Path> listFilesSorted(String directory) {
        List<Path> fileNames = new ArrayList<>();

        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {

            for (Path path : directoryStream) {
                //pokud udaj neni directory (tzn je soubor), tak ho pridame do pole
                if ( ! Files.isDirectory(path) && fileMatchesToRegex(path) ) {
                    fileNames.add(path);
                }
            }

            Collections.sort(fileNames, new Comparator<Path>() {
                public int compare(Path o1, Path o2) {
                    int result = 0;
                    try {
                        result = o2.getFileName().compareTo(o1.getFileName());

                        if (result == 0) {
                            result = Files.getLastModifiedTime(o1).compareTo(Files.getLastModifiedTime(o2));
                        }

                    } catch (IOException e) {
                        logger.error("Unable to sort files ", e);
                    }
                    return result;
                }
            });

        } catch (NoSuchFileException ex) {
            logger.error("Unable to list files, directory " + directory + "doesn't exist", ex);
            //ex.printStackTrace();
        } catch (IOException ex) {
            logger.error("test", ex);
        } catch (Exception ex) {
            //System.out.println("Unable to list files");
            logger.error("Unable to list files ", ex);
            ex.printStackTrace();
        } finally {
            //System.out.println("Moving files finished");
            if (fileNames.size() >= 1 ) {
                logger.info("Listing files finished: " + fileNames);
            } else {
                logger.info("Listing files finished, no file found");
            }
        }
        return fileNames;
    }

    public static boolean fileMatchesToRegex(Path path) {
        //jak tohle opravit do smysluplnyho try catche
        Boolean bool = false;

        try {
            String path_string = path.toString();
            if (path_string.matches(Properties.fileRegex) == true) {
                bool = true;
                logger.debug("File name " + path_string + " matches to regex 'source.file.regex'");
            } else {
                bool = false;
                logger.debug("File name " + path_string + " doesn't match to regex 'source.file.regex'");
            }
        } catch (Exception e) {
            logger.error("Error while matching files to regex", e);
        } finally {
            if (bool == null) {
                bool = false;
            }
        }
        return bool;
    }

    /**
     * Urceni poctu souboru k presunuti
     * @param listOfFiles
     * @return pocet souboru k presunuti
     */
    public static int getNumberOfFilesToMove(List<String> listOfFiles) {
        int import_threads_int;
        int filesToMove = 0;

        try {
            Boolean isDayHour = Main.isDayHour();

            if (isDayHour) {
                logger.trace("Property 'import.threads.number.day' value is " + Properties.importThreadsNumberDay);
                import_threads_int = Integer.parseInt(Properties.importThreadsNumberDay);
            } else {
                logger.trace("Property 'import.threads.number.night' value is " + Properties.importThreadsNumberNight);
                import_threads_int = Integer.parseInt(Properties.importThreadsNumberNight);
            }

            //select aktualniho poctu bezicich vlaken. Pokud je v properties db.fake=true, potom se vole fake metoda, ktera vraci pevny integer
            boolean db_fake = Main.getDBFakeProperty();
            int int_record_import_count;
            if (db_fake == true) {
                int_record_import_count = DB.selectRecordsImportCountFake();
                logger.error("Property db.fake is set to true! Only static integer is returned: " + int_record_import_count);
            } else {
                int_record_import_count = DB.selectRecordsImportCount();
                logger.trace("Property db.fake is set to false " + int_record_import_count);
            }

            logger.trace("Number of running threads is " + int_record_import_count);

            filesToMove = import_threads_int - int_record_import_count;

            logger.trace("Number of free threads is " + filesToMove + " (" + import_threads_int + " - " + int_record_import_count + ")");

            //rozhodnout o velikosti pole na zaklade poctu souboru ke kopirovani
            if (filesToMove >= listOfFiles.size()) {
                filesToMove = listOfFiles.size();
            }
        } catch (Exception e) {
            logger.error("Unable to get number of files to move: ", e);
        }
        return filesToMove;
    }

/* asi nebude implementovano
    public static int getNumberOfFilesToMove2(List<String> listOfFiles) {
        int numberOfFilesToMove = 0;
        int recordsImportThreadsDBConfig;

        try {
            Boolean isDayHour = cz.csas.sas_espis.filemover.Main.isDayHour();

            if (isDayHour) {
                recordsImportThreadsDBConfig = cz.csas.sas_espis.filemover.DB.selectRecordsImportThreadsDayFake();
            } else {
                recordsImportThreadsDBConfig = cz.csas.sas_espis.filemover.DB.selectRecordsImportThreadsNightFake();
            }

            int recordsImportThreadsDBCount = cz.csas.sas_espis.filemover.DB.selectRecordsImportCountFake();
            int importThreadsNumberProperty = Integer.parseInt(cz.csas.sas_espis.filemover.Properties.importThreadsNumber);

            //vymyslet
            numberOfFilesToMove = recordsImportThreadsDBConfig - ( recordsImportThreadsDBCount - importThreadsNumberProperty );

        } catch (Exception e) {
            logger.error("Unable to get number of files to move: ", e);
        }
        return numberOfFilesToMove;
    }
*/

}
