package cz.csas.sas_espis.filemover;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.util.password.BasicPasswordEncryptor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by sol60831 on 12.5.2016.
 * Nacitani properties
 */
public class Properties {

    private static final Logger logger = LogManager.getLogger(Properties.class);
    private static final String propertyFile = "config/filemover.properties";
    public static final String importThreadsNumberDay = readProperty("import.threads.number.day");
    public static final String importThreadsNumberNight = readProperty("import.threads.number.night");
    public static final String fileRegex = Properties.readProperty("source.file.regex");
    public static final String DB_CONNECTION = Properties.readProperty("db.connection.string");
    public static final String DB_USER = Properties.readProperty("db.user");
    public static final String DB_PASSWORD = Properties.readProperty("db.password");

    //nacte volanou property z property souboru
    public static String readProperty(String property) {

        logger.trace("Property file is " + propertyFile);

        java.util.Properties prop = new java.util.Properties();
        InputStream input = null;
        String returnedProperty = "";

        try {
            input = new FileInputStream(propertyFile);

            // load a properties file
            prop.load(input);

            // get the property value
            returnedProperty = prop.getProperty(property);

            logger.trace("Property '{}' value is {}", property, returnedProperty);

        } catch (FileNotFoundException ex) {
            logger.error("Property file '{}' not found!", propertyFile, ex);
            System.exit(1);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return returnedProperty;
    }

    public static String readEncryptedProperty(String property) {

    }
}